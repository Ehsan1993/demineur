package Swing;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class CheckBox {

	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Check Box");
		JLabel label1, label2, label3, label4;
		JButton button = new JButton("Order");
		JCheckBox checkbox1, checkbox2, checkbox3;
		
			frame.setSize(300, 300);
			frame.setLayout(null);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			label1 = new JLabel("Food Ordering System");
			frame.add(label1);
			label1.setBounds(30, 30, 200, 30);
			
			label2 = new JLabel("Pizza = 100$");
			frame.add(label2);
			label2.setBounds(60, 70, 100, 20);
			
			label3 = new JLabel("Burger = 30$");
			frame.add(label3);
			label3.setBounds(60, 90, 100, 20);
			
			label4 = new JLabel("Tea = 10$");
			frame.add(label4);
			label4.setBounds(60, 110, 100, 20);
			
			checkbox1 = new JCheckBox();
			frame.add(checkbox1);
			checkbox1.setBounds(40, 70, 20, 20);
			
			checkbox2 = new JCheckBox();
			frame.add(checkbox2);
			checkbox2.setBounds(40, 90, 20, 20);
			
			checkbox3 = new JCheckBox();
			frame.add(checkbox3);
			checkbox3.setBounds(40, 110, 20, 20);
			
			
			frame.add(button);
			button.setBounds(90, 160, 80, 30);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					float amount = 0;
					String sms = "";
					if (checkbox1.isSelected()) {
						amount += 100;
						sms = "pizza: 100\n";
					}
					if (checkbox2.isSelected()) {
						amount += 30;
						sms += "Burger: 30\n";
					}
					if (checkbox3.isSelected()) {
						amount += 10;
						sms += "Tea: 10\n";
					}
					sms += "----------------\n";
				
					JOptionPane.showMessageDialog(null, sms + "Total: " + amount + "$" );

				}
			});
	

		
	}
}
