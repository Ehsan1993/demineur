package Swing;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class TextFieldExample implements ActionListener {

	JFrame frame = new JFrame("Text Field Example");
	JTextField textfield1, textfield2, textfield3;
	JButton button1, button2;
	
	public TextFieldExample(){
		
		textfield1 = new JTextField();
		textfield1.setBounds(50, 50, 150, 20);
		textfield2 = new JTextField();
		textfield2.setBounds(50, 100, 150, 20);
		textfield3 = new JTextField();
		textfield3.setBounds(50, 150, 150, 20);
		//textfield3.setEditable(false);
		
		button1 = new JButton("+");
		button1.setBounds(50, 200, 45, 30);
		button2 = new JButton("-");
		button2.setBounds(120, 200, 45, 30);
		button1.addActionListener(this);
		button2.addActionListener(this);
		
		frame.add(button1); 
		frame.add(button2); 
		frame.add(textfield1); 
		frame.add(textfield2); 
		frame.add(textfield3);
	
		frame.setVisible(true);
		frame.setSize(300,300);
		frame.setLayout(null);
		
	}
	
	//@Override
	public void actionPerformed(ActionEvent e) {

		String S1 = textfield1.getText();
		String S2 = textfield2.getText();
		
		int a = Integer.parseInt(S1);
		int b = Integer.parseInt(S2);
		int c = 0;
		
		if (e.getSource()==button1) {
			c = a + b;
		}
		else if(e.getSource()==button2) {
			c = a - b;
		}
		String result = String.valueOf(c);
		
		JOptionPane.showMessageDialog(null,
                result,
                "result",
                JOptionPane.INFORMATION_MESSAGE);
		
		textfield3.setText(result);
		
	}
	public static void main(String[] args) {  
	    new TextFieldExample(); 
	}

}

