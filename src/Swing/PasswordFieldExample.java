package Swing;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class PasswordFieldExample {
	
	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Password Field Example");
		JPasswordField password = new JPasswordField();
		JLabel label1, label2, label3;
		JTextField textfield = new JTextField();
		JButton button = new JButton("Login");
		
			frame.setSize(300, 300);
			frame.setLayout(null);
			frame.setVisible(true);
			
			label1 = new JLabel("Username:");
			frame.add(label1);
			label1.setBounds(20, 50, 80, 30);
			
			label2 = new JLabel("Password:");
			frame.add(label2);
			label2.setBounds(20, 100, 80, 30);
			
			label3 = new JLabel();
			frame.add(label3);
			label3.setBounds(20, 180, 200, 50);
			
			frame.add(textfield);
			textfield.setBounds(100, 50, 100, 30);
			
			frame.add(password);
			password.setBounds(100, 100, 100, 30);
			
			frame.add(button);
			button.setBounds(110, 150, 80, 30);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String data = "Username " + textfield.getText();
					data += ", Password: " + new String(password.getPassword());
					label3.setText(data);
				}
			});
		
	

		
	}

}
