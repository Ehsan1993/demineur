package Demineur;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class JFrameClass implements ActionListener, MouseListener {

	int rows = 10;
	int columns = 10;
	int numberOfMines = 20;
	boolean [][] mines = new boolean[rows][columns];
	boolean [][] rightClick = new boolean [rows][columns];
	boolean [][] leftClick = new boolean [rows][columns];
	int [][] mineExisted = new int [rows][columns];
	int mineCounter = 0;
	int marked = 0;


	JFrame frame = new JFrame("Démineur");
	JButton [][] button = new JButton[rows][columns];
	JPanel panel = new JPanel();
	JMenuBar menuBar = new JMenuBar();
	JMenu menu1, submenu;
	JMenuItem menuItem1, menuItem2, menuItem3, menuItem4, menuItem5;
	JLabel label = new JLabel("Mines: " +numberOfMines+ " Marked: "+marked);



	public JFrameClass() {

		frame.setJMenuBar(menuBar);
		menuBar.setBackground(Color.LIGHT_GRAY);
		frame.setResizable(false);
		panel.setBackground(Color.LIGHT_GRAY);

		menu1 = new JMenu("File");
		submenu = new JMenu("Difficulty");
		menuItem1 = new JMenuItem("New Game");
		menuItem2 = new JMenuItem("Easy");
		menuItem3 = new JMenuItem("Medium");
		menuItem4 = new JMenuItem("Difficult");
		menuItem5 = new JMenuItem("Quit");
		menu1.add(menuItem1);
		menu1.add(submenu);
		menu1.add(menuItem5);
		submenu.add(menuItem2);
		submenu.add(menuItem3);
		submenu.add(menuItem4);

		// Functions for New Game
		menuItem1.addActionListener(this);
		// Functions for Easy
		menuItem2.addActionListener(this);
		// Functions for Medium
		menuItem3.addActionListener(this);
		// Functions for Difficult
		menuItem4.addActionListener(this);
		// Functions for Quite
		menuItem5.addActionListener(this);

		menuBar.add(menu1);
		frame.add(label);
		frame.add(panel);
		GridLayout layout = new GridLayout(rows, columns);
		panel.setLayout(layout);
		frame.setSize(450, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);




		Buttons();		
		Mine();


	}


	private void Mine() {

		//to complete the panel with the random mines

		while (numberOfMines > 0) {
			int i = (int) (Math.random() * rows);
			int j = (int) (Math.random() * columns);
			if (!mines[i][j]) {
				mines[i][j] = true;
				numberOfMines--;
			}
		}

		//to locate the mines and actions of buttons

		for (int i =0; i<rows; i++) {
			for (int j=0; i<columns; j++) {
				if (mines[i][j] == true) {
					mineExisted[i][j] = -1;
				}
				if (mines[i][j] == false) {
					if (i-1>=0 && j-1>=0){
						if (mines[i-1][j-1] == true) mineCounter++;
					}
					if (i-1>=0){
						if (mines[i-1][j] == true) mineCounter++;
					}
					if (i-1>=0 && j+1<columns){
						if (mines[i-1][j+1] == true) mineCounter++;
					}
					if (j-1>=0) {
						if (mines[i][j-1] == true) mineCounter++;
					}
					if (j+1<columns){
						if (mines[i][j+1] == true) mineCounter++;
					}
					if (i+1<rows && j-1>=0) {
						if (mines[i+1][j-1] == true) mineCounter++;
					}	
					if (i+1<rows){
						if (mines[i+1][j] == true) mineCounter++;
					}
					if (i+1<rows && j+1<columns){
						if (mines[i+1][j+1] == true) mineCounter++;
					}
				}
				mineExisted[i][j] = mineCounter;
			}
		}
	}

	private void ClickMouse(int i, int j) {

		switch (mineExisted[i][j]) {
		// if we click a mine
		case 1:
			for (i = 0; i<rows; i++) {
				for (j = 0; j<columns; j++) {
					if(mines[i][j] == true) {
						button[i][j].setText("X");
					}
				}
				//		lose = true;
				break;	
			}
			// if 
		case 2:





		}
	}

	private void Buttons() {

		//for adding buttons in the panel

		for (int i = 0; i<rows; i++) {
			for (int j=0; j<columns;j++)
			{
				button[i][j]=new JButton();
				panel.add(button[i][j]);
				button[i][j].setBounds(25, 25, 0, 0);
				button[i][j].addActionListener(this);
				button[i][j].addMouseListener(this);
				rightClick[i][j] = false;
				leftClick[i][j] = true;
			}		
		}
	} 



	@Override
	public void actionPerformed(ActionEvent e) {

		if (menuItem5 == e.getSource()) {
			System.exit(0);
		}

		// Functions for Difficulty button

		if (menuItem2 == e.getSource()) {
			int rows = 10;
			int columns = 10;
			int numberOfMines = 20;
			frame.setSize(450, 500);
			reset();
		}
		if (menuItem3 == e.getSource()) {
			int rows = 15;
			int columns = 20;
			int numberOfMines = 70;
			frame.setSize(700, 550);
			reset();
		}
		if (menuItem4 == e.getSource()) {
			int rows = 20;
			int columns = 30;
			int numberOfMines = 150;
			frame.setSize(950, 800);
			reset();
		}
	}

	private void reset() {
	}

	private void NewGame() {

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				button[i][j].setEnabled(true);
				button[i][j].setText("");
				button[i][j].setIcon(null);
				mines[i][j] = false;
				leftClick[i][j] = true;
				rightClick[i][j] = false;
				mineExisted[i][j] = 0;
				marked = 0;
				label.setText("Mines: " + numberOfMines + " Marked: " + marked);
				//	Win = false;
				//	Lose = false;
			}
			Mine();
			Buttons();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		// Actions for the right Click
		if (e.getButton() == 3 ) {
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < columns; j++) {
					if (e.getSource() == button[i][j] && rightClick[i][j] == false) {
						button[i][j].setText("*");
						rightClick[i][j] = true;
						marked++;
					}
					else if (e.getSource() == button [i][j] && rightClick[i][j] == true) {
						button[i][j].setText("");
						rightClick[i][j] = false;	
						marked--;
					}
					label.setText("Mines: " + numberOfMines + " Marked: " + marked);
				}
			}
		}

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	void lost(){
		JOptionPane.showMessageDialog(null,"Game Over");
	}
	void win() {
		JOptionPane.showMessageDialog(null, "You won the game.\nNew Game?");
	}
}

